<%@page import="java.sql.Date"%>
<%@page import="forum.ForumThread"%>
<%@page import="forum.Post"%>
<%@page import="forum.Comment"%>
<%@page import="forum.Category"%>
<%@page import="postgreSQLDatabase.forum.Query"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IIIT KOTA | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
 
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<input type="hidden" id="thread_id" value="<%=Long.parseLong(request.getParameter("thread_id")) %>"/>
<span id="post_delete" style="display:none">

                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
                  <i class="fa fa-circle-o"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool deletePost"><i class="fa fa-times"></i></button>
              
</span>

<span id="comment_delete" style="display:none">
  <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
                  <i class="fa fa-circle-o"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool deleteComment"><i class="fa fa-times"></i></button>
              
</span>

<span id="comment" style="display:none">
<div class="box-comment">
                <!-- User image -->
                <img class="img-circle img-sm" src="../dist/img/user3-128x128.jpg" alt="User Image">
               
                
            

                <div class="comment-text">
                    <span class="username"><span class="comment_username"></span> 
                        
                        <span class="text-muted pull-right">
                        <span class="comment_timestamp">    </span>        
                        
                        
              <span class="comment_buttons"></span>
                        </span>
                        </span> 
                      <!-- /.username -->
                  <span class="comment_data"></span>
                </div>
 </div>
</span>
<span id="post" style="display:none">
    	<div class="row">
    		<div class="col-md-8">
          <div class="box box-widget">
            
            <div class="box-header with-border">
            
              <div class="user-block">
                
                <img class="img-circle" src="../dist/img/user1-128x128.jpg" alt="User Image">
                <span class="username">
                <span class="post_username"><a href="#"></a></span></span>
               <span class="description"><span class="time"></span></span> 
              </div>
              
              
              <div class="box-tools">
              <span class="post_buttons"></span> 
              </div>
              
            </div>
            <div class="box-body">
              <!-- post text -->
              <p><span class="post_name"></span></p>

            </div>
            <!-- /.box-body -->
            <div class="box-footer box-comments" style="background-color:lightgray;">
           
           <span class="comments"></span>
              
           
           
              
              
            </div>
            <div class="box-footer">
              
                <img class="img-responsive img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="Alt Text">
                <div class="img-push">
                  <input type="text" class="form-control input-sm addComment" 
                  placeholder="Press enter to post comment" name="comment" onkeypress="addComment(event,this,post_id)">
                </div>
              
            </div>
            </div>
    </div>
    </div>
    </span>
<div class="wrapper">
<%@ include file="header.jsp" %>
 <!-- Left side column. contains the logo and sidebar -->
 <%@ include file="main-sidebar.jsp" %>
 
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      <%Long thread_id=Long.parseLong(request.getParameter("thread_id"));
      ForumThread thread=Query.getSingleThread(thread_id);
      Long category_id=thread.getParent_category_id();
      String thread_name=thread.getThread_name();
      Date time=thread.getTimestamp();
      Long author_id=thread.getAuthor_id();
      String author_name=Query.getAuthorName(author_id);
      String category_name=Query.getCategoryName(category_id); %>
        FORUMS<small><%=category_name %></small>
        <label> <%=thread_name %></label><br>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#" class="active"><i class="fa fa-dashboard"></i>Home</a></li>
        
      </ol>
    </section>
      <section class="content">
    <% ArrayList<Post> post=postgreSQLDatabase.forum.Query.getMultiplePosts(thread_id);
    Iterator<Post> iterator=post.iterator();
    while(iterator.hasNext()){iterator.next();if(1+1==2)continue;
		Post current=iterator.next();
		Long post_author_id=current.getAuthor_id();
		String post_author_name=Query.getAuthorName(post_author_id);
		
    %>
    <!-- Main content -->
  
    
    	<div class="row">
    		<div class="col-md-8">
          <div class="box box-widget">
            
            <div class="box-header with-border">
            
              <div class="user-block">
                
                <img class="img-circle" src="../dist/img/user1-128x128.jpg" alt="User Image">
                <span class="username"><a href="#"><%=post_author_name %></a></span>
                <span class="time"><%=time %></span>
              </div>
              
              <%Long erp_id=Long.parseLong(String.valueOf(session.getAttribute("erpId")));
              if(current.getAuthor_id()==erp_id){
              %>
              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
                  <i class="fa fa-circle-o"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool"  onclick="deletePost(this,<%=current.getPost_id()%>,<%=current.getAuthor_id()%>)"><i class="fa fa-times"></i></button>
              </div>
              <%} %>
            </div>
            <div class="box-body">
              <!-- post text -->
              <%=current.getPost_name() %>

            </div>
            <!-- /.box-body -->
            <div class="box-footer box-comments" style="background-color:lightgray;">
            <%Long post_id=current.getPost_id();
            ArrayList<Comment> comment=Query.getMultipleComments(post_id);
            Iterator<Comment> iter=comment.iterator();
            while(iter.hasNext()){
        		Comment com=iter.next();
           %>
           <div class="box-comment">
                <!-- User image -->
                <img class="img-circle img-sm" src="../dist/img/user3-128x128.jpg" alt="User Image">
               <%if(erp_id==com.getAuthor_id()){ %>
                <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
                  <i class="fa fa-circle-o"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" onclick="deleteComment(<%=com.getComment_id()%>,<%=com.getAuthor_id()%>)"><i class="fa fa-times"></i></button>
              </div>
              <%} %>

                <div class="comment-text">
                      <span class="username">
                        <%Long comment_author_id=com.getAuthor_id();
                        String comment_author_name=Query.getAuthorName(comment_author_id);
                        String data=com.getData();
                        
                        %>
                       
                        <%=comment_author_name %>
                        <span class="text-muted pull-right"><%=com.getTimestamp() %></span>
                      </span><!-- /.username -->
                  <%=data %>
                </div>
              </div>
              
           
           <%} %>
              
              
            </div>
            <div class="box-footer">
              
                <img class="img-responsive img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="Alt Text">
                <div class="img-push">
                  <input type="text" class="form-control input-sm" 
                  placeholder="Press enter to post comment" name="comment" onkeypress="addComment(event,this,<%=current.getPost_id()%>)">
                </div>
              
            </div>
            </div>
    </div>
    </div>
    
    <%} %><span id="feed"></span>
    <div class="row ">
    <div class="col-md-8">
    <form action="../AddNewPost?thread_id=<%= thread_id %>" method="post">
										<div class="modal-content">
											<div class="modal-header">
												<div class="timeline-footer pull-right">
												</div>
												
												<h4 class="modal-title">
												
												</h4>
											</div>
											
											<div class="modal-body">
												<div class="form-group">
													<textarea type="text" class="form-control" style="border-top:none !important;border-right:none !important;border-left:none !important;height:100px !important;resize:none !important;"id="exampleInputEmail1" placeholder="Your Post..." name="post_name"></textarea>								
													<br />
													
													<div class="btn-group pull-right">
														<button class="btn btn-primary btn-xs btn-info">Send Post</button>
													</div>
                 								</div>
											</div>
											
											<div class="modal-body pull-right">
											</div>
										</div>
										
										</form>
										
            </div>
          </div>
        
    	

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <%@ include file="footer.jsp" %>
  <!-- Control Sidebar -->
  <%@ include file="control-sidebar.jsp" %>
 </div>
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
  
  function update(thread_id){
	  var xmlhttp;
	  try{
		  xmlhttp = new XMLHttpRequest ();
	  }
	  catch(e){
		  
		  try{
			  xmlhttp = new ActiveXObject(" msxml2.XMLHTTP");
		  }
		  catch(e){
			  try{
				  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  }
			  catch(e){
				  alert("Your browser is unsupported");
			  }
		  }
	  }
	  if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		        	
		        	createPost(xmlhttp.responseText);
		        	//location.reload();	
				}
		        
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    xmlhttp.open("POST","../RefreshThread",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("thread_id="+thread_id);
		    }

  }
  
  function createPost(object){
	  document.getElementById("feed").innerHTML="";
	  var json=JSON.parse(object);
	  for(i=0;i<json.length;i++){
		  //alert(JSON.stringify(json[i]));
		  var post_template=document.getElementById("post");
		  
		  post_template.getElementsByClassName("form-control input-sm addComment")[0].setAttribute("onkeypress","javascript:addComment(event,this,"+json[i]["post_id"]+")");
		  post_template.getElementsByClassName("post_name")[0].innerHTML=json[i]["post_name"];
		  post_template.getElementsByClassName("post_username")[0].innerHTML=json[i]["post_author_name"];
		  post_template.getElementsByClassName("time")[0].innerHTML=json[i]["timestamp"];
		  post_template.getElementsByClassName("post_buttons")[0].innerHTML="";
		  if(json[i]["post_author"]==true){
			  var button_template=document.getElementById("post_delete");
			  button_template.getElementsByClassName("btn btn-box-tool deletePost")[0].setAttribute("onclick","javascript:deletePost("+json[i]["post_id"]+","+json[i]["post_author_id"]+")");
			  post_template.getElementsByClassName("post_buttons")[0].innerHTML+=button_template.innerHTML;
			  		  
		  }
		  var comment_json=json[i]["comments"];
		  post_template.getElementsByClassName("comments")[0].innerHTML="";
		  for(j=0;j<comment_json.length;j++){
			  var comment_template=document.getElementById("comment");
			  
			  comment_template.getElementsByClassName("comment_username")[0].innerHTML=comment_json[j]["comment_author_name"];
			  comment_template.getElementsByClassName("comment_data")[0].innerHTML=comment_json[j]["data"];
			  comment_template.getElementsByClassName("comment_timestamp")[0].innerHTML=comment_json[j]["timestamp"];
			  comment_template.getElementsByClassName("comment_buttons")[0].innerHTML="";
			  if(comment_json[j]["comment_author"]==true){
				  var button1_template=document.getElementById("comment_delete");
				  button1_template.getElementsByClassName("btn btn-box-tool deleteComment")[0].setAttribute("onclick","javascript:deleteComment("+comment_json[j]["comment_id"]+","+comment_json[j]["comment_author_id"]+")");
				  comment_template.getElementsByClassName("comment_buttons")[0].innerHTML+=button1_template.innerHTML;
				  		  
			  }
			  post_template.getElementsByClassName("comments")[0].innerHTML+=comment_template.innerHTML;
		  }
		  
		  document.getElementById("feed").innerHTML+=post_template.innerHTML;
		 // alert(json[i]["post_id"]);
		  //alert(json[i]["post_name"]);
		  //alert(json[i]["post_author_id"]);
		  //alert(json[i]["timestamp"]);
		  //alert(json[i]["comments"]);
		  
		  
	  }
  }
  
  function deleteComment(comment_id,author_id){
	  var xmlhttp;
	  try{
		  xmlhttp = new XMLHttpRequest ();
	  }
	  catch(e){
		  
		  try{
			  xmlhttp = new ActiveXObject(" msxml2.XMLHTTP");
		  }
		  catch(e){
			  try{
				  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  }
			  catch(e){
				  alert("Your browser is unsupported");
			  }
		  }
	  }
	  if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		        	update(document.getElementById("thread_id").value);	
		        	
				}
		        
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    //alert("hello");
		    xmlhttp.open("POST","../DeleteComment",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("comment_id="+comment_id+"&author_id="+author_id);
		    }

  }
  
  function deletePost(post_id,author_id){
	  var xmlhttp;
	  try{
		  xmlhttp = new XMLHttpRequest ();
	  }
	  catch(e){
		  
		  try{
			  xmlhttp = new ActiveXObject(" msxml2.XMLHTTP");
		  }
		  catch(e){
			  try{
				  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  }
			  catch(e){
				  alert("Your browser is unsupported");
			  }
		  }
	  }
	  if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		        	
		        	
		        	update(document.getElementById("thread_id").value);	
				}
		        
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    xmlhttp.open("POST","../DeletePost",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("post_id="+post_id+"&author_id="+author_id);
		    }

  }
  function addComment(e,input,post_id){
	  if(e.keyCode==13){
		  //alert(input.value);
		  //alert(post_id);
		  var xmlhttp;
			try{
				xmlhttp = new XMLHttpRequest();
			} catch (e){
				// Internet Explorer Browsers
				try{
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try{
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e){
					//Browser doesn't support ajax	
						alert("Your browser is unsupported");
					}
				}
			}	
			
			if(xmlhttp){
			    xmlhttp.onreadystatechange=function() {
			    	
			        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			        	
			        	
			        	update(document.getElementById("thread_id").value);	
					}
			        
			        if(xmlhttp.status == 404)
						alert("Could not connect to server");
					}
			    xmlhttp.open("POST","../AddNewComment",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			    xmlhttp.send("text="+input.value+"&post_id="+post_id);
			    }
	  }
  }
  update(document.getElementById("thread_id").value);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
</body>
</html>