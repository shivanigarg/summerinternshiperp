CKEDITOR.plugins.add( 'tags',
{   
   requires : ['richcombo'], //, 'styles' ],
   init : function( editor )
   {
      var config = editor.config,
         lang = editor.lang.format;

      // Gets the list of tags from the settings.
      var tags = []; //new Array();
      //this.add('value', 'drop_text', 'drop_label');
      tags[0]=["{(student-name)}", "Student Name", "Student Name"];
      tags[1]=["{(student-id)}", "Student Id", "Student Id"];
      tags[2]=["{(Date)}", "Date", "Date"];
     
      // Create style objects for all defined styles.
      editor.ui.addRichCombo( 'tags',
         {
            label : "Insert Tags",
            title :"Insert Tags",
            voiceLabel : "Insert Tag",
            className : 'cke_format',
            multiSelect : false,

            panel :
            {
            	
            	//css: [ CKEDITOR.getUrl('skins/moono/editor.css')],
               voiceLabel : lang.panelVoiceLabel
            },

            init : function()
            {
               this.startGroup( "" );
               //this.add('value', 'drop_text', 'drop_label');
               for (var this_tag in tags){
                  this.add(tags[this_tag][0], tags[this_tag][1], tags[this_tag][2]);
               }
            },

            onClick : function( value )
            {         
               editor.focus();
               editor.fire( 'saveSnapshot' );
               editor.insertHtml(value);
               editor.fire( 'saveSnapshot' );
            }
         });
   }
});