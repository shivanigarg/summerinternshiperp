<!DOCTYPE html>
<%@page import="postgreSQLDatabase.registration.Query"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="users.Student"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIIT KOTA | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">

<!-- jvectormap -->
<link rel="stylesheet"
	href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Date Picker -->
<link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
<!-- Daterange picker -->
<link rel="stylesheet"
	href="../plugins/daterangepicker/daterangepicker-bs3.css">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet"
	href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="index2.html" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>IIIT</b>K</span> <!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>IIITK </b>ERP</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- Messages: style can be found in dropdown.less-->
						<li class="dropdown messages-menu"><a href="#"
							class="dropdown-toggle" data-toggle="dropdown"> <i
								class="fa fa-envelope-o"></i> <span class="label label-success">4</span>
						</a>
							<ul class="dropdown-menu">
								<li class="header">You have 4 messages</li>
								<li>
									<!-- inner menu: contains the actual data -->
									<ul class="menu">
										<li>
											<!-- start message --> <a href="#">
												<div class="pull-left">
													<img src="../dist/img/user2-160x160.jpg" class="img-circle"
														alt="User Image">
												</div>
												<h4>
													Support Team <small><i class="fa fa-clock-o"></i> 5
														mins</small>
												</h4>
												<p>Why not buy a new awesome theme?</p>
										</a>
										</li>
										<!-- end message -->
										<li><a href="#">
												<div class="pull-left">
													<img src="../dist/img/user3-128x128.jpg" class="img-circle"
														alt="User Image">
												</div>
												<h4>
													AdminLTE Design Team <small><i
														class="fa fa-clock-o"></i> 2 hours</small>
												</h4>
												<p>Why not buy a new awesome theme?</p>
										</a></li>
										<li><a href="#">
												<div class="pull-left">
													<img src="../dist/img/user4-128x128.jpg" class="img-circle"
														alt="User Image">
												</div>
												<h4>
													Developers <small><i class="fa fa-clock-o"></i>
														Today</small>
												</h4>
												<p>Why not buy a new awesome theme?</p>
										</a></li>
										<li><a href="#">
												<div class="pull-left">
													<img src="../dist/img/user3-128x128.jpg" class="img-circle"
														alt="User Image">
												</div>
												<h4>
													Sales Department <small><i class="fa fa-clock-o"></i>
														Yesterday</small>
												</h4>
												<p>Why not buy a new awesome theme?</p>
										</a></li>
										<li><a href="#">
												<div class="pull-left">
													<img src="../dist/img/user4-128x128.jpg" class="img-circle"
														alt="User Image">
												</div>
												<h4>
													Reviewers <small><i class="fa fa-clock-o"></i> 2
														days</small>
												</h4>
												<p>Why not buy a new awesome theme?</p>
										</a></li>
									</ul>
								</li>
								<li class="footer"><a href="#">See All Messages</a></li>
							</ul></li>
						<!-- Notifications: style can be found in dropdown.less -->
						<li class="dropdown notifications-menu"><a href="#"
							class="dropdown-toggle" data-toggle="dropdown"> <i
								class="fa fa-bell-o"></i> <span class="label label-warning">10</span>
						</a>
							<ul class="dropdown-menu">
								<li class="header">You have 10 notifications</li>
								<li>
									<!-- inner menu: contains the actual data -->
									<ul class="menu">
										<li><a href="#"> <i class="fa fa-users text-aqua"></i>
												5 new members joined today
										</a></li>
										<li><a href="#"> <i class="fa fa-warning text-yellow"></i>
												Very long description here that may not fit into the page
												and may cause design problems
										</a></li>
										<li><a href="#"> <i class="fa fa-users text-red"></i>
												5 new members joined
										</a></li>
										<li><a href="#"> <i
												class="fa fa-shopping-cart text-green"></i> 25 sales made
										</a></li>
										<li><a href="#"> <i class="fa fa-user text-red"></i>
												You changed your username
										</a></li>
									</ul>
								</li>
								<li class="footer"><a href="#">View all</a></li>
							</ul></li>
						<!-- Tasks: style can be found in dropdown.less -->
						<li class="dropdown tasks-menu"><a href="#"
							class="dropdown-toggle" data-toggle="dropdown"> <i
								class="fa fa-flag-o"></i> <span class="label label-danger">9</span>
						</a>
							<ul class="dropdown-menu">
								<li class="header">You have 9 tasks</li>
								<li>
									<!-- inner menu: contains the actual data -->
									<ul class="menu">
										<li>
											<!-- Task item --> <a href="#">
												<h3>
													Design some buttons <small class="pull-right">20%</small>
												</h3>
												<div class="progress xs">
													<div class="progress-bar progress-bar-aqua"
														style="width: 20%" role="progressbar" aria-valuenow="20"
														aria-valuemin="0" aria-valuemax="100">
														<span class="sr-only">20% Complete</span>
													</div>
												</div>
										</a>
										</li>
										<!-- end task item -->
										<li>
											<!-- Task item --> <a href="#">
												<h3>
													Create a nice theme <small class="pull-right">40%</small>
												</h3>
												<div class="progress xs">
													<div class="progress-bar progress-bar-green"
														style="width: 40%" role="progressbar" aria-valuenow="20"
														aria-valuemin="0" aria-valuemax="100">
														<span class="sr-only">40% Complete</span>
													</div>
												</div>
										</a>
										</li>
										<!-- end task item -->
										<li>
											<!-- Task item --> <a href="#">
												<h3>
													Some task I need to do <small class="pull-right">60%</small>
												</h3>
												<div class="progress xs">
													<div class="progress-bar progress-bar-red"
														style="width: 60%" role="progressbar" aria-valuenow="20"
														aria-valuemin="0" aria-valuemax="100">
														<span class="sr-only">60% Complete</span>
													</div>
												</div>
										</a>
										</li>
										<!-- end task item -->
										<li>
											<!-- Task item --> <a href="#">
												<h3>
													Make beautiful transitions <small class="pull-right">80%</small>
												</h3>
												<div class="progress xs">
													<div class="progress-bar progress-bar-yellow"
														style="width: 80%" role="progressbar" aria-valuenow="20"
														aria-valuemin="0" aria-valuemax="100">
														<span class="sr-only">80% Complete</span>
													</div>
												</div>
										</a>
										</li>
										<!-- end task item -->
									</ul>
								</li>
								<li class="footer"><a href="#">View all tasks</a></li>
							</ul></li>
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu"><a href="#"
							class="dropdown-toggle" data-toggle="dropdown"> <img
								src="../dist/img/user2-160x160.jpg" class="user-image"
								alt="User Image"> <span class="hidden-xs">Alexander
									Pierce</span>
						</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header"><img
									src="../dist/img/user2-160x160.jpg" class="img-circle"
									alt="User Image">

									<p>
										Alexander Pierce - Web Developer <small>Member since
											Nov. 2012</small>
									</p></li>
								<!-- Menu Body -->
								<li class="user-body">
									<div class="row">
										<div class="col-xs-4 text-center">
											<a href="#">Followers</a>
										</div>
										<div class="col-xs-4 text-center">
											<a href="#">Sales</a>
										</div>
										<div class="col-xs-4 text-center">
											<a href="#">Friends</a>
										</div>
									</div> <!-- /.row -->
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="#" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul></li>
						<!-- Control Sidebar Toggle Button -->
						<li><a href="#" data-toggle="control-sidebar"><i
								class="fa fa-gears"></i></a></li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="../dist/img/user2-160x160.jpg" class="img-circle"
							alt="User Image">
					</div>
					<div class="pull-left info">
						<p>
							<%
								out.print(request.getSession().getAttribute("name"));
							%>
						</p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- search form -->
				<form action="#" method="get" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="q" class="form-control"
							placeholder="Search..."> <span class="input-group-btn">
							<button type="submit" name="search" id="search-btn"
								class="btn btn-flat">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>
				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Student <small>Home</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#" class="active"><i class="fa fa-dashboard"></i>Home</a></li>

				</ol>
			</section>

			<!-- Modal -->

			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Student Details</h4>
						</div>
						<div class="modal-body">

							<table id="modal_table"
								class="table table-bordered table-striped">
								<tbody>

									<tr>
										<td>Registration Id</td>
										<td><input type=text class="form-control"
											id="registration_id"></td>
									</tr>


									<tr>
										<td>Name</td>
										<td><input type="text" id="name" class="form-control"
											value=""></td>
									</tr>
									<tr>
										<td>First Name</td>
										<td><input type="text" id="first_name"
											class="form-control" value=""></td>
									</tr>
									<tr>
										<td>Middle Name</td>
										<td><input type="text" id="middle_name"
											class="form-control" value=""></td>
									</tr>
									<tr>
										<td>Last Name</td>
										<td><input type="text" id="last_name"
											class="form-control" value=""></td>
									</tr>

									<tr>
										<td>Category</td>
										<td><input type="text" class="form-control" id="category"></td>
									</tr>
									<tr>
										<td>JEE Main Roll No.</td>
										<td><input type="text" class="form-control"
											id="jee_main_roll_no"></td>
									</tr>
									<tr>
										<td>JEE Advanced Roll No.</td>
										<td><input type="text" class="form-control"
											id="jee_advanced_roll_no"></td>
									</tr>
									<tr>
										<td>State</td>
										<td><input type="text" class="form-control" id="state"></td>
									</tr>
									<tr>
										<td>Phone Number</td>
										<td><input type="text" class="form-control" id="phone_no"></td>
									</tr>
									<tr>
										<td>Email</td>
										<td><input type="text" class="form-control" id="email"></td>
									</tr>
									<tr>
										<td>Date Of Birth</td>
										<td><input type="text" class="form-control"
											id="date_of_birth"></td>
									</tr>
									<tr>
										<td>Program Allocated</td>
										<td><input type="text" class="form-control"
											id="program_allocated"></td>
									</tr>
									<tr>
										<td>Allocated Category</td>
										<td><input type="text" class="form-control"
											id="allocated_category"></td>
									</tr>
									<tr>
										<td>Allocated Rank</td>
										<td><input type="text" class="form-control"
											id="allocated_rank"></td>
									</tr>
									<tr>
										<td>Status</td>
										<td><input type="text" class="form-control" id="status"></td>
									</tr>
									<tr>
										<td>Choice Number</td>
										<td><input type="text" class="form-control"
											id="choice_number"></td>
									</tr>
									<tr>
										<td>Physically Disabled</td>
										<td><input type="text" class="form-control"
											id="physically_disabled"></td>
									</tr>
									<tr>
										<td>Gender</td>
										<td><input class="form-control" class="form-control"
											type="text" id="gender"></td>
									</tr>
									<tr>
										<td>Quota</td>
										<td><input type="text" class="form-control" id="quota"></td>
									</tr>
									<tr>
										<td>Round</td>
										<td><input class="form-control" class="form-control"
											type="text" id="round"></td>
									</tr>
									<tr>
										<td>Willingness</td>
										<td><input type="text" class="form-control"
											id="willingness"></td>
									</tr>
									<tr>
										<td>Address</td>
										<td><input type="text" class="form-control" id="address"></td>
									</tr>
									<tr>
										<td>RC Name</td>
										<td><input type="text" class="form-control" id="rc_name"></td>
									</tr>
									<tr>
										<td>Nationality</td>
										<td><input type="text" class="form-control"
											id="nationality"></td>
									</tr>
								</tbody>
							</table>

						</div>
						<div class="modal-footer">
							<button type="button" onclick="update()" class="btn btn-warning"
								data-dismiss="modal" id="update_data">Update</button>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>

						</div>
					</div>
				</div>
			</div>

			<!-- Main content -->
<!-- Modal 2 begin-->
<!-- Modal -->

			<div class="modal fade" id="myModalToAdd" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Student Details</h4>
						</div>
						<div class="modal-body">

							<table id="modal_table"
								class="table table-bordered table-striped">
								<tbody>

									<tr>
										<td>Registration Id</td>
										<td><input type=text class="form-control"
											id="insert_registration_id"></td>
									</tr>


									<tr>
										<td>Name</td>
										<td><input type="text" id="insert_name" class="form-control"
											value=""></td>
									</tr>
									<tr>
										<td>First Name</td>
										<td><input type="text" id="insert_first_name"
											class="form-control" value=""></td>
									</tr>
									<tr>
										<td>Middle Name</td>
										<td><input type="text" id="insert_middle_name"
											class="form-control" value=""></td>
									</tr>
									<tr>
										<td>Last Name</td>
										<td><input type="text" id="insert_last_name"
											class="form-control" value=""></td>
									</tr>

									<tr>
										<td>Category</td>
										<td><input type="text" class="form-control" id="insert_category"></td>
									</tr>
									<tr>
										<td>JEE Main Roll No.</td>
										<td><input type="text" class="form-control"
											id="insert_jee_main_roll_no"></td>
									</tr>
									<tr>
										<td>JEE Advanced Roll No.</td>
										<td><input type="text" class="form-control"
											id="insert_jee_advanced_roll_no"></td>
									</tr>
									<tr>
										<td>State</td>
										<td><input type="text" class="form-control" id="insert_state"></td>
									</tr>
									<tr>
										<td>Phone Number</td>
										<td><input type="text" class="form-control" id="insert_phone_no"></td>
									</tr>
									<tr>
										<td>Email</td>
										<td><input type="text" class="form-control" id="insert_email"></td>
									</tr>
									<tr>
										<td>Date Of Birth</td>
										<td><input type="text" class="form-control"
											id="insert_date_of_birth"></td>
									</tr>
									<tr>
										<td>Program Allocated</td>
										<td><input type="text" class="form-control"
											id="insert_program_allocated"></td>
									</tr>
									<tr>
										<td>Allocated Category</td>
										<td><input type="text" class="form-control"
											id="insert_allocated_category"></td>
									</tr>
									<tr>
										<td>Allocated Rank</td>
										<td><input type="text" class="form-control"
											id="insert_allocated_rank"></td>
									</tr>
									<tr>
										<td>Status</td>
										<td><input type="text" class="form-control" id="insert_status"></td>
									</tr>
									<tr>
										<td>Choice Number</td>
										<td><input type="text" class="form-control"
											id="insert_choice_number"></td>
									</tr>
									<tr>
										<td>Physically Disabled</td>
										<td><input type="text" class="form-control"
											id="insert_physically_disabled"></td>
									</tr>
									<tr>
										<td>Gender</td>
										<td><input class="form-control" class="form-control"
											type="text" id="insert_gender"></td>
									</tr>
									<tr>
										<td>Quota</td>
										<td><input type="text" class="form-control" id="insert_quota"></td>
									</tr>
									<tr>
										<td>Round</td>
										<td><input class="form-control" class="form-control"
											type="text" id="insert_round"></td>
									</tr>
									<tr>
										<td>Willingness</td>
										<td><input type="text" class="form-control"
											id="insert_willingness"></td>
									</tr>
									<tr>
										<td>Address</td>
										<td><input type="text" class="form-control" id="insert_address"></td>
									</tr>
									<tr>
										<td>RC Name</td>
										<td><input type="text" class="form-control" id="insert_rc_name"></td>
									</tr>
									<tr>
										<td>Nationality</td>
										<td><input type="text" class="form-control"
											id="insert_nationality"></td>
									</tr>
								</tbody>
							</table>

						</div>
						<div class="modal-footer">
							<button type="button" onclick="add()" class="btn btn-danger"
								data-dismiss="modal" id="update_data">Add</button>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>

						</div>
					</div>
				</div>
			</div>


<!--  Modal 2 end -->


			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Student List</h3>
								<button type="button" class="btn btn-block btn-default"
									data-toggle="modal" data-target="#myModalToAdd"
									>
									<i class="glyphicon glyphicon-eye-open"></i>
								</button>
							</div>
							<!-- /.box-header -->
							<div class="box-body" style="overflow-x: scroll;">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>View</th>
											<th>Delete</th>
											<th>Name</th>
											<th>JEE Main Roll No.</th>
											<th>State</th>
											<th>Phone Number</th>
											<th>Email</th>
											<th>Program Allocated</th>
											<th>Allocated Category</th>
											<th>Physically Disabled</th>
											<th>Entry Date</th>
											<th>Reported</th>
										</tr>
									</thead>
									<tbody>
										<%
											ArrayList<Student> csab_list=postgreSQLDatabase.registration.Query.getCsabStudentList();
										                                Iterator<Student> iterator=csab_list.iterator();
										                                while(iterator.hasNext()){
										                    				users.Student current=iterator.next();
										%>
										<tr>
											<td><button type="button"
													class="btn btn-block btn-default" data-toggle="modal"
													data-target="#myModal"
													onclick="displayProfile(<%=current.getCsab_id()%>)">
													<i class="glyphicon glyphicon-eye-open"></i>
												</button></td>
											<td><button type="button"
													class="btn btn-block btn-danger"
													onclick="deleteCSABStudent(<%=current.getCsab_id()%>)">
													<i class="glyphicon glyphicon-trash"></i>
												</button></td>

											<td><%=current.getName()%></td>
											<td><%=current.getJee_main_rollno()%></td>
											<td><%=current.getState_eligibility()%></td>
											<td><%=current.getMobile()%></td>
											<td><%=current.getEmail()%></td>
											<td><%=current.getProgram_allocated()%></td>
											<td><%=current.getAllocated_category()%></td>
											<td><%=current.isPwd()%></td>
											<td><%=current.getEntry_date()%></td>
											<td><%=current.isReported()%></td>
										</tr>
										<%
											}
										%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- Main content -->

			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 0.0.1
			</div>
			<strong>Copyright &copy; 2016 <a
				href="http://iiitkota.ac.in">IIITK</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<!-- Create the tabs -->
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i
						class="fa fa-home"></i></a></li>
				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
						class="fa fa-gears"></i></a></li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<!-- Home tab content -->
				<div class="tab-pane" id="control-sidebar-home-tab">
					<h3 class="control-sidebar-heading">Recent Activity</h3>
					<ul class="control-sidebar-menu">
						<li><a href="javascript::;"> <i
								class="menu-icon fa fa-birthday-cake bg-red"></i>

								<div class="menu-info">
									<h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

									<p>Will be 23 on April 24th</p>
								</div>
						</a></li>
						<li><a href="javascript::;"> <i
								class="menu-icon fa fa-user bg-yellow"></i>

								<div class="menu-info">
									<h4 class="control-sidebar-subheading">Frodo Updated His
										Profile</h4>

									<p>New phone +1(800)555-1234</p>
								</div>
						</a></li>
						<li><a href="javascript::;"> <i
								class="menu-icon fa fa-envelope-o bg-light-blue"></i>

								<div class="menu-info">
									<h4 class="control-sidebar-subheading">Nora Joined Mailing
										List</h4>

									<p>nora@example.com</p>
								</div>
						</a></li>
						<li><a href="javascript::;"> <i
								class="menu-icon fa fa-file-code-o bg-green"></i>

								<div class="menu-info">
									<h4 class="control-sidebar-subheading">Cron Job 254
										Executed</h4>

									<p>Execution time 5 seconds</p>
								</div>
						</a></li>
					</ul>
					<!-- /.control-sidebar-menu -->

					<h3 class="control-sidebar-heading">Tasks Progress</h3>
					<ul class="control-sidebar-menu">
						<li><a href="javascript::;">
								<h4 class="control-sidebar-subheading">
									Custom Template Design <span
										class="label label-danger pull-right">70%</span>
								</h4>

								<div class="progress progress-xxs">
									<div class="progress-bar progress-bar-danger"
										style="width: 70%"></div>
								</div>
						</a></li>
						<li><a href="javascript::;">
								<h4 class="control-sidebar-subheading">
									Update Resume <span class="label label-success pull-right">95%</span>
								</h4>

								<div class="progress progress-xxs">
									<div class="progress-bar progress-bar-success"
										style="width: 95%"></div>
								</div>
						</a></li>
						<li><a href="javascript::;">
								<h4 class="control-sidebar-subheading">
									Laravel Integration <span
										class="label label-warning pull-right">50%</span>
								</h4>

								<div class="progress progress-xxs">
									<div class="progress-bar progress-bar-warning"
										style="width: 50%"></div>
								</div>
						</a></li>
						<li><a href="javascript::;">
								<h4 class="control-sidebar-subheading">
									Back End Framework <span class="label label-primary pull-right">68%</span>
								</h4>

								<div class="progress progress-xxs">
									<div class="progress-bar progress-bar-primary"
										style="width: 68%"></div>
								</div>
						</a></li>
					</ul>
					<!-- /.control-sidebar-menu -->

				</div>
				<!-- /.tab-pane -->
				<!-- Stats tab content -->
				<div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab
					Content</div>
				<!-- /.tab-pane -->
				<!-- Settings tab content -->
				<div class="tab-pane" id="control-sidebar-settings-tab">
					<form method="post">
						<h3 class="control-sidebar-heading">General Settings</h3>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Report panel
								usage <input type="checkbox" class="pull-right" checked>
							</label>

							<p>Some information about this general settings option</p>
						</div>
						<!-- /.form-group -->

						<div class="form-group">
							<label class="control-sidebar-subheading"> Allow mail
								redirect <input type="checkbox" class="pull-right" checked>
							</label>

							<p>Other sets of options are available</p>
						</div>
						<!-- /.form-group -->

						<div class="form-group">
							<label class="control-sidebar-subheading"> Expose author
								name in posts <input type="checkbox" class="pull-right" checked>
							</label>

							<p>Allow the user to show his name in blog posts</p>
						</div>
						<!-- /.form-group -->

						<h3 class="control-sidebar-heading">Chat Settings</h3>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Show me as
								online <input type="checkbox" class="pull-right" checked>
							</label>
						</div>
						<!-- /.form-group -->

						<div class="form-group">
							<label class="control-sidebar-subheading"> Turn off
								notifications <input type="checkbox" class="pull-right">
							</label>
						</div>
						<!-- /.form-group -->

						<div class="form-group">
							<label class="control-sidebar-subheading"> Delete chat
								history <a href="javascript::;" class="text-red pull-right"><i
									class="fa fa-trash-o"></i></a>
							</label>
						</div>
						<!-- /.form-group -->
					</form>
				</div>
				<!-- /.tab-pane -->
			</div>
		</aside>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- Morris.js charts -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="../plugins/morris/morris.min.js"></script>
	<!-- Sparkline -->
	<script src="../plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- jQuery Knob Chart -->
	<script src="../plugins/knob/jquery.knob.js"></script>
	<!-- daterangepicker -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="../plugins/daterangepicker/daterangepicker.js"></script>
	<!-- DataTables -->
	<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- datepicker -->
	<script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script
		src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<!-- Slimscroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

	<!-- AdminLTE for demo purposes -->
	<script src="../dist/js/demo.js"></script>
	<script>
  $(function () {
    $("#example1").DataTable({
		"paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
	});
  });
  
  
  function displayProfile(csab_id){
		var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();

		if(xmlhttp){	
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
					var data=JSON.parse(xmlhttp.responseText);
					alert(xmlhttp.responseText);
					document.getElementById("name").value=data.name;
					document.getElementById("category").value=data.category;
					document.getElementById("jee_main_roll_no").value=data.jee_main_rollno;
					document.getElementById("jee_advanced_roll_no").value=data.jee_adv_rollno;
					document.getElementById("state").value=data.state;
					document.getElementById("phone_no").value=data.phone_number;
					document.getElementById("email").value=data.email;
					document.getElementById("date_of_birth").value=data.date_of_birth;
					document.getElementById("program_allocated").value=data.program_allocated;
					document.getElementById("allocated_category").value=data.allocated_category;
					document.getElementById("allocated_rank").value=data.allocated_rank;
					document.getElementById("status").value=data.status;
					document.getElementById("choice_number").value=data.choice_no;
					if(data.physically_disabled==true)
					document.getElementById("physically_disabled").value="Yes";
					else
						document.getElementById("physically_disabled").value="No";
					
					document.getElementById("gender").value=data.gender;
					document.getElementById("quota").value=data.quota;
					document.getElementById("round").value=data.round;
					document.getElementById("willingness").value=data.willingness;
					document.getElementById("address").value=data.permanent_address;
					document.getElementById("rc_name").value=data.rc_name;
					document.getElementById("nationality").value=data.nationality;
				//	document.getElementById("entry_date").value=data.entry_date;
					//document.getElementById("reported").value=data.reported;
					if(data.reported==false){
						document.getElementById("registration_id").value="Student has not yet reported";
					}
					else{
						document.getElementById("registration_id").value="Registration ID- "+data.registration_id;
					}
					document.getElementById("update_data").setAttribute("onclick","javascript:update("+csab_id+")");									
					
									
				}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			xmlhttp.open("POST","../CsabStudentProfile",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("csab_id="+csab_id);
		}
		return false;

	}
  
  
  function update(csab_id){
		var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();


		if(xmlhttp){
			var data= new FormData();
			data.append("csab_id",csab_id);
			data.append("name",document.getElementById("name").value);
			data.append("first_name",document.getElementById("first_name").value);
			data.append("middle_name",document.getElementById("middle_name").value);
			data.append("last_name",document.getElementById("last_name").value);
			data.append("category",document.getElementById("category").value);
			data.append("jee_main_rollno",document.getElementById("jee_main_roll_no").value);
			data.append("jee_advance_rollno",document.getElementById("jee_advanced_roll_no").value);
			data.append("state",document.getElementById("state").value);
			data.append("phone_number",document.getElementById("phone_no").value);
			data.append("email",document.getElementById("email").value);
			data.append("date_of_birth",document.getElementById("date_of_birth").value);
			data.append("program_allocated",document.getElementById("program_allocated").value);
			data.append("allocated_category",document.getElementById("allocated_category").value);
			data.append("allocated_rank",document.getElementById("allocated_rank").value);
			data.append("status",document.getElementById("status").value);
			data.append("choice_number",document.getElementById("choice_number").value);
			if(data.physically_disabled==true)
			data.append("pwd",document.getElementById("physically_disabled").value);
			else
				data.append("pwd",document.getElementById("physically_disabled").value);
			
			data.append("gender",document.getElementById("gender").value);
			data.append("quota",document.getElementById("quota").value);
			data.append("round",document.getElementById("round").value);
			data.append("willingness",document.getElementById("willingness").value);
			data.append("address",document.getElementById("address").value);
			data.append("rc_name",document.getElementById("rc_name").value);
			data.append("nationality",document.getElementById("nationality").value);
			//document.getElementById("reported").value);
			if(data.reported==false){
				data.append("isReported","Not reported yet");
			}
			else{
				data.append("isReported",document.getElementById("registration_id").value);
			}
					
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			xmlhttp.open("POST","../UpdateCSABStudentData?action=update",true);
			//xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			var outputLog = {}, iterator =data.entries(), end = false;
			while(end == false) {
			  var item = iterator.next();
			  if(item.value!=undefined) {
			      outputLog[item.value[0]] = item.value[1];
			  } else if(item.done==true) {
			      end = true;
			  }
			   }
			console.log(outputLog);
			                              
			                xmlhttp.send(data);
			            }
		return false;

	}

function add(){
	alert("reached");
		var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();


		if(xmlhttp){
			var data= new FormData();
			alert("name"+document.getElementById("insert_name").value);
			data.append("name",document.getElementById("insert_name").value);
			data.append("first_name",document.getElementById("insert_first_name").value);
			data.append("middle_name",document.getElementById("insert_middle_name").value);
			data.append("last_name",document.getElementById("insert_last_name").value);
			data.append("category",document.getElementById("insert_category").value);
			data.append("jee_main_rollno",document.getElementById("insert_jee_main_roll_no").value);
			data.append("jee_advance_rollno",document.getElementById("insert_jee_advanced_roll_no").value);
			data.append("state",document.getElementById("insert_state").value);
			data.append("phone_number",document.getElementById("insert_phone_no").value);
			data.append("email",document.getElementById("insert_email").value);
			data.append("date_of_birth",document.getElementById("insert_date_of_birth").value);
			data.append("program_allocated",document.getElementById("insert_program_allocated").value);
			data.append("allocated_category",document.getElementById("insert_allocated_category").value);
			data.append("allocated_rank",document.getElementById("insert_allocated_rank").value);
			data.append("status",document.getElementById("insert_status").value);
			data.append("choice_number",document.getElementById("insert_choice_number").value);
			if(data.physically_disabled==true)
			data.append("pwd",document.getElementById("insert_physically_disabled").value);
			else
				data.append("pwd",document.getElementById("insert_physically_disabled").value);
			
			data.append("gender",document.getElementById("insert_gender").value);
			data.append("quota",document.getElementById("insert_quota").value);
			data.append("round",document.getElementById("insert_round").value);
			data.append("willingness",document.getElementById("insert_willingness").value);
			data.append("address",document.getElementById("insert_address").value);
			data.append("rc_name",document.getElementById("insert_rc_name").value);
			data.append("nationality",document.getElementById("insert_nationality").value);
			//document.getElementById("reported").value);
			
				data.append("registration_id",document.getElementById("insert_registration_id").value);
			
					
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			xmlhttp.open("POST","../UpdateCSABStudentData?action=add",true);
			//xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			var outputLog = {}, iterator =data.entries(), end = false;
			while(end == false) {
			  var item = iterator.next();
			  if(item.value!=undefined) {
			      outputLog[item.value[0]] = item.value[1];
			  } else if(item.done==true) {
			      end = true;
			  }
			   }
			console.log(outputLog);
			                              
			                xmlhttp.send(data);
			            }
		return false;

	}

  
  
  function deleteCSABStudent(csab_id){
	  var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();

		if(xmlhttp){	
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
					window.location.reload();
					//alert(xmlhttp.responseText);
							}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			xmlhttp.open("POST","../DeleteCSABStudent",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("csab_id="+csab_id);
		}
		return false;
	  
  }

</script>
</body>
</html>
