<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIIT KOTA | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Office <small>Home</small>
			</h1>

			<ol class="breadcrumb">
				<li><a href="#" class="active"><i class="fa fa-dashboard"></i>Home</a></li>

			</ol>
			</section>
			<form method="POST" action="../SaveTemplates">
				<input type=text " name="title" />
				<!-- Main content -->
				<!-- /.content -->
				<textarea cols="80" rows="10" id="content" name="contents"> 
    </textarea>
				<input type="text" name="name"> <input type="submit" />

			</form>

			<button onclick="getHTML()">Save</button>
			<form method="POST" action="newPage.jsp">
				<button>Next</button>
			</form>
			<script type="text/javascript">
			CKEDITOR.on('instanceReady', function() {;
			var iframe = $('#content').contents ();
			iframe.find ('html').css ({
			'background-color': '#b0b0b0'
			});
			iframe.find ('body').css ({
			'width': '297mm',
			'height': '210mm',
			'background-color': '#ffffff',
			'margin': '0mm',
			'padding': '5mm'
			});
			});
			
				function getHTML() {
					var file = CKEDITOR.instances.content.getData();
					alert(file);
					document.getElementById("content").innerHTML = file;
				}
			</script>

		</div>

		<!-- /.content-wrapper -->
		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script src="../plugins/ckeditor/ckeditor.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- Slimscroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>

	<!-- AdminLTE for demo purposes -->
	<script src="../dist/js/demo.js"></script>
	<!-- 


	<textarea cols="80" rows="10" id="content" name="contents"> 
    </textarea>
 -->
	<script type="text/javascript">
		CKEDITOR.replace('content', {
			toolbar : 'Basic',
			uiColor : '#9AB8FC'
		});
		CKEDITOR.instances.content.setData();
		function getHTML() {
			alert(CKEDITOR.instances.content.getData());
		}
	</script>

</body>
</html>