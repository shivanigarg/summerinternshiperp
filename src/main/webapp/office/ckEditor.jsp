<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CKEDITOR</title>
<style>
body {
  background: rgb(204,204,204); 
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
  overflow:hidden;
}
page[size="A4"][layout="portrait"] {  
  width: 21cm;
  height: 29.7cm; 
}
page[size="A4"][layout="landscape"]{
  width: 29.7cm;
  height: 21cm;  
}

@media print {
  body, page {
    margin: 0;
    box-shadow: 0;
  }
}
</style>

</head>
<body>
   <page size="A4" layout="portrait"><div id="editor1" contenteditable="true" style="margin: 2cm 2cm 2cm 2cm;" ></div> </page>

<input type ="hidden" value="heya" id="test"/>
</body>
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script src="../plugins/ckeditor/ckeditor.js"></script>
	
<script>

CKEDITOR.disableAutoInline = true;
CKEDITOR.inline( 'editor1',{extraPlugins: 'tags'});


</script>
</html>