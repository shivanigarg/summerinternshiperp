<!DOCTYPE html>
<%@page import="postgreSQLDatabase.registration.Query"%>
<%@page import="csv.Parser"%>
<%@page import="java.io.File"%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIITK | ERP</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- DataTables -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- This is the Javascript file of jqGrid -->
<style>
invisible {
    display: none  !important;
    visibility:hidden  !important;
   

}
visible {
    display: inline;
}
</style>

<script>
	function skip(input){
		var num=input.value;
		table_body = document.getElementById("csv").getElementsByClassName("table_body")[0];
		rows = table_body.getElementsByTagName("tr").length;
		
		if(num>=(rows) || num<0){
			alert("Incorrect number of rows");
			return false;
		}
		else{
			for(i=0;i<rows;i++){
				
				if(num!=0){
					table_body.getElementsByTagName("tr")[i].setAttribute('style', 'display:none !important');
					num--;
				}
				else{
					table_body.getElementsByTagName("tr")[i].setAttribute('style', 'display:table-row !important');
				}
			}
			
		}
	}
</script>


<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>
		<script src="../dist/js/reportStudent.js"></script>
		<%@ page import="java.util.ArrayList"%>
		<%@ page import="java.util.Iterator"%>
		<%@ page import="users.Student"%>



		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					CSAB <small>Student Registration List</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Tables</a></li>
					<li class="active">Data tables</li>
				</ol>
			</section>



			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Student List</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body" style="overflow-x: scroll;">
								<%
									String filename=request.getServletContext().getRealPath("")+File.separator+"upload"+File.separator+request.getParameter("filename");
														                // out.println(filename);
														                Parser csv=new Parser(filename);
														                
								%>
								<div class="pull-right">
								<label>Skip Rows</label>
									<input id="skip_rows" type="number" min="0" onchange=" return skip(this)" value="0"
										placeholder="Skip rows" />
								</div>
								<button type="button" onclick="getColumns()">Save</button>
								<table id="csv" class="table table-bordered table-striped">
									<thead>
										<tr>
											<%
											out.println("<th>SN</th>");
												for(int i=1;i<=csv.getNumCols();i++)
														{
																				%>
											<th><select class="field">
													<option value="ignore">Ignore</option>
													<option value="student_id">Student ID</option>
													<option value="name">Name</option>
													<option value="first_name">First Name</option>
													<option value="middle_name">Middle Name</option>
													<option value="last_name">Last Name</option>
													<option value="category">Category</option>
													<option value="jee_main_rollno">JEE Main Roll No</option>
													<option value="jee_adv_rollno">JEE Advance Roll No</option>
													<option value="state">State</option>
													<option value="phone_number">Phone Number</option>
													<option value="email">Email</option>
													<option value="date_of_birth">Date Of Birth</option>
													<option value="program_allocated">Program
														Allocated</option>
													<option value="allocated_category">Allocated
														Category</option>
													<option value="allocated_rank">Allocated Rank</option>
													<option value="status">Status</option>
													<option value="choice_no">Choice Number</option>
													<option value="physically_disabled">Physically
														Disabled</option>
													<option value="gender">Gender</option>
													<option value="quota">Quota</option>
													<option value="round">Round</option>
													<option value="willingness">Willingness</option>
													<option value="address">Address</option>
													<option value="rc_name">RC Name</option>
													<option value="nationality">Nationality</option>
											</select></th>
											<%	}
											%>
										</tr>
									</thead>
									<tbody class="table_body">
									<%
											ArrayList<ArrayList<String>> array=csv.getArray();
																		   Iterator<ArrayList<String>> iterator=array.iterator();
																		   int count=1;
																		   while(iterator.hasNext()){
																			   
																			   out.println("<tr>");
																			   out.println("<td>"+ ++count+"</td>");
																			   ArrayList<String> current=iterator.next();
																				 Iterator<String> iterator1=current.iterator();
																			int col_count=1;
																				 while(iterator1.hasNext()){
																				col_count++;
																				out.println("<td>"+iterator1.next()+"</td>");
																			}
																				 while(col_count<=csv.getNumCols()){
																						col_count++;
																						out.println("<td></td>");
																					}
																				
																				out.println("</tr>");
																		   }
																	
										%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>

				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
		<!-- /.control-sidebar -->

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->

	<!-- Bootstrap 3.3.5 -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<!-- AdminLTE for demo purposes -->
	<!-- page script -->
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script>
	 $(function () {
		  var table=  $("#csv").DataTable({
			  "paging": true,
		      "lengthChange": true,
		      "searching": true,
		      "ordering": false,
		      "info": true,
		      "autoWidth": true
		      
			});
		  
		  });
	</script>
	<script type="text/javascript">
		
		function getColumns(){
			
			var array=[];
		//var mandatory_fields=["name","date_of_birth","state","category","program_allocated","gender","jee_main_rollno"];
	var mandatory_fields=["name"];	
		var skip_rows=document.getElementById("skip_rows").value;
			//alert("hello"+skip_rows);
			var columns=document.getElementById("csv").getElementsByClassName("field");
			for(var i=0;i<columns.length;i++){
				array.push(columns[i].value);
			}
			
			for(var j=0;j<mandatory_fields.length;j++){
				
				if(array.indexOf(mandatory_fields[j])==-1){alert(mandatory_fields[j]+" not found");return;}
			} 
			
			
			for(i=0;i<array.length;i++){
				for(j=i+1;j<array.length;j++){
					if(array[i]=="ignore")break;
					if(array[i]==array[j]){
						alert("Duplicate Entry of '"+array[j]+"' at the "+(j+1)+" column number");
						break;
					   }
				}
			}
			
			var xmlhttp;
			try{
				xmlhttp = new XMLHttpRequest();
			} catch (e){
				// Internet Explorer Browsers
				try{
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
					
				} catch (e) {
					try{
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e){
						//Browser doesn't support ajax	
						alert("Your browser is unsupported");
					}
				}
			}	

			if(xmlhttp){	
				xmlhttp.onreadystatechange=function() {
					if (xmlhttp.readyState==4 && xmlhttp.status==200) {
						var data=JSON.parse(xmlhttp.responseText);
						if(data.success){
							alert("File uploaded successfully");
						}
						else{
							alert(data.message);
						}
						//alert(xmlhttp.responseText);
					}
					if(xmlhttp.status == 404)
						alert("Could not connect to server");
				}
				xmlhttp.open("POST","../UploadCsabCsv",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send("array="+array+"&filename=<%=request.getParameter("filename")%>"+"&skip_start="+skip_rows);
			}
			return false;

			
		}
		document.getElementById('skip_rows').setAttribute('max', document.getElementsByTagName('tr').length-1);
	</script>
</body>
</html>