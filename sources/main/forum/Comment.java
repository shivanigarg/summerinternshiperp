/**
 * 
 */
package forum;

import java.sql.Date;

/**
 * @author Arushi
 *
 */
public class Comment {
	private Long comment_id;
	private String data;
	private Long parent_post_id;
	private Date timestamp;
	private Long author_id;
	/**
	 * @return the comment_id
	 */
	public Long getComment_id() {
		return comment_id;
	}
	/**
	 * @param comment_id the comment_id to set
	 */
	public void setComment_id(Long comment_id) {
		this.comment_id = comment_id;
	}
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}
	/**
	 * @return the parent_post_id
	 */
	public Long getParent_post_id() {
		return parent_post_id;
	}
	/**
	 * @param parent_post_id the parent_post_id to set
	 */
	public void setParent_post_id(Long parent_post_id) {
		this.parent_post_id = parent_post_id;
	}
	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	/**
	 * @return the author_id
	 */
	public Long getAuthor_id() {
		return author_id;
	}
	/**
	 * @param author_id the author_id to set
	 */
	public void setAuthor_id(Long author_id) {
		this.author_id = author_id;
	}

}
