/**
 * 
 */
package forum;

import java.sql.Date;

/**
 * @author Arushi
 *
 */
public class Post {
	private Long post_id;
	private Long parent_thread_id;
	private Long author_id;
	private Date timestamp;
	private String post_name;
	/**
	 * @return the post_id
	 */
	public Long getPost_id() {
		return post_id;
	}
	/**
	 * @param post_id the post_id to set
	 */
	public void setPost_id(Long post_id) {
		this.post_id = post_id;
	}
	/**
	 * @return the parent_thread_id
	 */
	public Long getParent_thread_id() {
		return parent_thread_id;
	}
	/**
	 * @param parent_thread_id the parent_thread_id to set
	 */
	public void setParent_thread_id(Long parent_thread_id) {
		this.parent_thread_id = parent_thread_id;
	}
	/**
	 * @return the author_id
	 */
	public Long getAuthor_id() {
		return author_id;
	}
	/**
	 * @param author_id the author_id to set
	 */
	public void setAuthor_id(Long author_id) {
		this.author_id = author_id;
	}
	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	/**
	 * @return the post_name
	 */
	public String getPost_name() {
		return post_name;
	}
	/**
	 * @param post_name the post_name to set
	 */
	public void setPost_name(String post_name) {
		this.post_name = post_name;
	}

}
