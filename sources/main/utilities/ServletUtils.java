/**
 * 
 */
package utilities;

/**
 * @author Joey
 *
 */

	import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;

import com.itextpdf.text.pdf.codec.Base64.InputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

	public class ServletUtils {

	  // constructors /////////////////////////////////////////////////////////////

	  // constants ////////////////////////////////////////////////////////////////

	  // classes //////////////////////////////////////////////////////////////////

	  // methods //////////////////////////////////////////////////////////////////

	  /**
	   * NOT UNIT TESTED Returns the URL (including query parameters) minus the scheme, host, and
	   * context path.  This method probably be moved to a more general purpose
	   * class.
	   */
	  public static String getRelativeUrl(
	    HttpServletRequest request ) {

	    String baseUrl = null;

	    if ( ( request.getServerPort() == 80 ) ||
	         ( request.getServerPort() == 443 ) )
	      baseUrl =
	        request.getScheme() + "://" +
	        request.getServerName() +
	        request.getContextPath();
	    else
	      baseUrl =
	        request.getScheme() + "://" +
	        request.getServerName() + ":" + request.getServerPort() +
	        request.getContextPath();

	    StringBuffer buf = request.getRequestURL();

	    if ( request.getQueryString() != null ) {
	      buf.append( "?" );
	      buf.append( request.getQueryString() );
	    }

	    return buf.substring( baseUrl.length() );
	  }

	  /**
	   * NOT UNIT TESTED Returns the base url (e.g, <tt>http://myhost:8080/myapp</tt>) suitable for
	   * using in a base tag or building reliable urls.
	   */
	  public static String getBaseUrl( HttpServletRequest request ) {
	    if ( ( request.getServerPort() == 80 ) ||
	         ( request.getServerPort() == 443 ) )
	      return request.getScheme() + "://" +
	             request.getServerName() +
	             request.getContextPath();
	    else
	      return request.getScheme() + "://" +
	             request.getServerName() + ":" + request.getServerPort() +
	             request.getContextPath();
	  }

	  /**
	   * Returns the file specified by <tt>path</tt> as returned by
	   * <tt>ServletContext.getRealPath()</tt>.
	   */
	  public static File getRealFile(
	    HttpServletRequest request,
	    String path ) {

	    return
	      new File( request.getSession().getServletContext().getRealPath( path ) );
	  }

	  public static String getFileName(Part part) {
	        String contentDisp = part.getHeader("content-disposition");
	        System.out.println("content-disposition header= "+contentDisp);
	        String[] tokens = contentDisp.split(";");
	        for (String token : tokens) {
	            if (token.trim().startsWith("filename")) {
	                return token.substring(token.indexOf("=") + 2, token.length()-1);
	            }
	        }
	        return "";
	    }
	  public static String guessMimeType(String filename,java.io.InputStream stream) throws IOException {
		  AutoDetectParser parser = new AutoDetectParser();
  	    org.apache.tika.detect.Detector detector = parser.getDetector();
  	    Metadata md = new Metadata();
  	
  		md.add(Metadata.RESOURCE_NAME_KEY, filename);
  	    MediaType mediaType = detector.detect(stream, md);
stream.close();
  		 System.out.println(mediaType.toString());
  		return mediaType.toString();
		}
	}

	   
	  

