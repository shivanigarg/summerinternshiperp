/**
 * 
 */
package postgreSQLDatabase.file;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import postgreSQLDatabase.feePayment.FeeBreakup;
import settings.database.PostgreSQLConnection;

/**
 * @author Shubhi
 *
 */
public class Query {

	public static Long addNewFile(File file) throws SQLException {
		PreparedStatement proc = PostgreSQLConnection.getConnection()
				.prepareStatement("SELECT public.\"addFile\"(?,?,?,?);");
		proc.setString(1, file.getDirectory().toString());
		proc.setString(2, file.getFile_name());
		proc.setString(3, file.getExtension());
		proc.setLong(4, file.getAuthor());
		System.out.println(proc.toString());
		ResultSet rs = proc.executeQuery();
		rs.next();
		return rs.getLong(1);
	}

	public static long getFileId(String file_name) {
		long file_id = 0;
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFileId\"(?);");
			proc.setString(1, file_name);
			ResultSet rs = proc.executeQuery();
			rs.next();
			file_id = rs.getLong(1);
			return file_id;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file_id;

	}

	public static File getAllFileData(long id) {
		File file = new File();
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getAllFileData\"(?);");
			proc.setLong(1, id);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();

				rs.next();
				
				file.setAuthor(rs.getLong("author"));

				file.setDirectory(rs.getString("directory"));
				file.setSubdirectory(rs.getString("subdirectory"));
				file.setExtension(rs.getString("extension"));
				file.setFile_name(rs.getString("file_name"));
				

			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file;

	}

	public static void main(String[] args) throws SQLException {
		File file = new File();

		file.setAuthor(123l);
		file.setDirectory("dir");
		file.setExtension(".txt");
		file.setFile_name("filename");
		// Long id = addNewFile(file);
		// System.out.println(getFileId("temp"));
		getAllFileData(70);

	}

}
