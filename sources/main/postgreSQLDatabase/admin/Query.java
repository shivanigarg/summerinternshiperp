/**
 * 
 */
package postgreSQLDatabase.admin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import exceptions.IncorrectFormatException;
import postgreSQLDatabase.feePayment.FeeBreakup;
import settings.database.PostgreSQLConnection;
import users.Student;
import users.User;

/**
 * @author Anita
 *
 */
public class Query {
	static Connection conn;
	private static PreparedStatement proc;
	public static void main(String[] args) throws SQLException, IncorrectFormatException {
		getStudentsList();
		getUsersList();
		
	}
	public static ArrayList<Student> getStudentsList() throws SQLException, IncorrectFormatException {
		ArrayList<Student> students_list= null;

		
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getStudentsList\"();");
			students_list = new ArrayList<Student>();
			ResultSet rs = proc.executeQuery();
			// System.out.println("hello");
			// System.out.println(proc);
		Student current=null;
			while(rs.next()){
				current=new Student();
				current.setStudent_id(rs.getString("student_id"));
				current.setName(rs.getString("name"));
				current.setSemester(rs.getInt("semester"));
				current.setBatch(rs.getString("batch"));
				students_list.add(current);
			}
			return students_list;
	
	}
	
	public static ArrayList<User> getUsersList() throws SQLException, IncorrectFormatException {
		ArrayList<User> users_list= null;

		
			PreparedStatement proc =  settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getUsersList\"();");
			users_list = new ArrayList<User>();
			ResultSet rs = proc.executeQuery();
			// System.out.println("hello");
		User current=null;
			while(rs.next()){
				current=new User();
				current.setUsername(rs.getString("username"));
				current.setUser_type(rs.getString("user_type"));
				current.setEmail(rs.getString("email"));
				current.setLast_seen(rs.getDate("last_seen"));
				users_list.add(current);
				
			}
			return users_list;
	
	}
	


		
	
	
	
	
}

	
	
	

