/**
 * 
 */
package postgreSQLDatabase.feePayment;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import settings.database.*;
import users.Student;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.postgresql.util.PGobject;

import exceptions.IncorrectFormatException;

/**
 * @author Shubhi
 *
 */
public class Query {

	private static PreparedStatement proc;

	public static ArrayList<Payment> getFeePaymentHistory(long user_id) {

		ArrayList<Payment> history_info = new ArrayList<Payment>();
		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFeePaymentHistory\"(?);");

			proc.setObject(1, user_id);
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);

			while (rs.next()) {

				Payment history = new Payment();
				history.setRef_no(rs.getInt("ref_no"));
				history.setComment(rs.getString("comment"));
				history.setDetails(new JSONObject(rs.getString("details")));
				history.setAmount(rs.getLong("amount"));
				history.setVerified(rs.getBoolean("verified"));
				history.setPayment_method(rs.getInt("payment_method"));
				history_info.add(history);
			}
			rs.close();
			proc.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return history_info;
	}

	public static ArrayList<FeeBreakup> getFeePaymentList() {

		ArrayList<FeeBreakup> payment_list = new ArrayList<FeeBreakup>();
		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFeePaymentList\"();");

			ResultSet rs = proc.executeQuery();
			System.out.println(proc);

			while (rs.next()) {

				FeeBreakup breakup = new FeeBreakup();
				breakup.setCategory(rs.getString("category"));
				breakup.setTotal_amt(rs.getInt("total_amt"));
				breakup.setSemester(rs.getInt("semester"));
				breakup.setYear(rs.getInt("year"));
				JSONArray j_array = new JSONArray();
				Array breakup_array = rs.getArray("breakup");
				System.out.println(breakup_array);
				ResultSet as = breakup_array.getResultSet();
				while (as.next()) {
					j_array.put(new JSONObject(as.getString(2)));
				}
				breakup.setBreakup(j_array);
				payment_list.add(breakup);
			}
			System.out.println();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return payment_list;

	}

	public static FeePaymentDetails getFeePaymentDetails(long user_id) {
		FeePaymentDetails payment_details = new FeePaymentDetails();
		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFeePaymentDetails\"(?);");

			proc.setObject(1, user_id);
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			rs.next();

			payment_details.setId(rs.getInt("ref_no"));
			payment_details.setName(rs.getString("name"));
			JSONObject details = new JSONObject(rs.getString("details"));
			payment_details.setDetails(details);
			payment_details.setPayment_method(rs.getInt("payment_method"));
			rs.close();
			proc.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return payment_details;
	}

	public static void addFeeBreakup(int semester, String category, String breakup, int year) {
		try {

			JSONArray fee_breakup = new JSONArray(breakup);
			JSONObject amt_obj = fee_breakup.getJSONObject(fee_breakup.length() - 1);
			JSONObject j_array[] = new JSONObject[fee_breakup.length()];
			for (int i = 0; i < fee_breakup.length(); i++) {
				j_array[i] = fee_breakup.getJSONObject(i);

			}
			int amount = amt_obj.getInt("total");
			// System.out.println(amount);
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"addFeeBreakup\"(?,?,?,?,?);");
			proc.setInt(1, year);
			proc.setInt(2, semester);
			proc.setString(3, category);
			proc.setObject(4, PostgreSQLConnection.getConnection().createArrayOf("json", j_array));
			proc.setInt(5, amount);
			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static JSONArray getFeeJson(Long reg_id) {

		try {
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getFeeJson\"(?);");
			proc.setLong(1, reg_id);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			rs.next();
			JSONArray j_array = new JSONArray();
			ResultSet as = rs.getArray(1).getResultSet();
			while (as.next()) {
				JSONObject current = new JSONObject(as.getString(2));
				j_array.put(current);
			}
			return j_array;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public static ArrayList<FeeBreakup> getFeeBreakup(int semester, int year) {

		ArrayList<FeeBreakup> getList = new ArrayList<FeeBreakup>();
		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFeeBreakup\"(?,?);");
			proc.setInt(1, semester);
			proc.setInt(2, year);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();

			while (rs.next()) {
				FeeBreakup feebreakup = new FeeBreakup();
				feebreakup.setCategory(rs.getString("category"));
				feebreakup.setSemester(rs.getInt("semester"));
				feebreakup.setYear(rs.getInt("year"));
				feebreakup.setTotal_amt(rs.getInt("total_amt"));
				JSONArray breakup = new JSONArray();
				ResultSet as = rs.getArray("breakup").getResultSet();
				while (as.next()) {
					JSONObject json_object = new JSONObject(as.getString(2));
					breakup.put(json_object);
				}
				feebreakup.setBreakup(breakup);
				getList.add(feebreakup);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return getList;
	}
	// insert into payment table

	public static int addFeePayment(String comment, long pay_method, JSONObject details, long amt, long tid, long payee,
			long beneficiary) {

		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"addFeePayment\"(?,?,?,?,?,?,?);");

			proc.setString(1, comment);
			proc.setLong(2, pay_method);
			PGobject jsonObject = new PGobject();
			jsonObject.setType("json");
			jsonObject.setValue(details.toString());

			proc.setObject(3, jsonObject);

			proc.setLong(4, amt);
			proc.setLong(5, tid);
			proc.setLong(6, payee);
			proc.setLong(7, beneficiary);
			// System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			rs.next();
			// System.out.println(rs.getInt(1));
			return rs.getInt(1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

	public static void addFeeTransaction(int semester, String category, long reg_id, int year) {

		try {
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"addFeeTransaction\"(?,?,?,?);");

			proc.setInt(1, semester);

			proc.setString(2, category);

			proc.setLong(3, reg_id);
			proc.setInt(4, year);
			proc.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void verifyFeePayment(long ref_no) {

		try {
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"feeVerify\"(?);");
			proc.setLong(1, ref_no);
			proc.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static long retrieveFeeAmount(Long reg_id) {

		try {
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"getFeeAmount\"(?);");

			proc.setLong(1, reg_id);
			// System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			rs.next();
			// System.out.println(rs.getLong(1));
			return rs.getLong(1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

	public static void main(String[] args) throws SQLException, IncorrectFormatException {

		// ArrayList<Payment> fee=getFeePaymentHistory(1);
		// getFeeBreakup(1);
		// System.out.println(getFeePaymentDetails(69));
		// getFeeJson(69);
		getTransactionId(2016, 6, 1000000106);
		// getFeeBreakup(2, 2016);
	}

	public static long getTransactionId(int year, int sem, long reg_id) throws SQLException, IncorrectFormatException {
		long tid = 0l;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getTransactionId\"(?,?,?);");
			proc.setInt(2, sem);
			proc.setInt(1, year);
			proc.setLong(3, reg_id);

			ResultSet rs = proc.executeQuery();
			rs.next();
			tid = rs.getLong(1);

			rs.close();
			proc.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return tid;
	}

	public static JSONObject getFeeJson(int reg_id) throws SQLException {
		PreparedStatement proc = PostgreSQLConnection.getConnection()
				.prepareStatement("SELECT public.\"getFeeJson\"(?);");
		try {
			proc.setObject(1, reg_id);
			ResultSet rs = proc.executeQuery();
			rs.next();
			String fee = rs.getString(1);
			JSONObject fee_breakup = new JSONObject(fee);
			return fee_breakup;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * @param string
	 * @param i
	 * @param details
	 * @param parseInt
	 * @param parseLong
	 * @param attribute
	 * @return
	 */

}
