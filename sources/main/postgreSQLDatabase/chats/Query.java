/**
 * 
 */
package postgreSQLDatabase.chats;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;





//import actions.chats.Conversation;
import settings.database.PostgreSQLConnection;

/**
 * @author Shubhi
 *
 */
public class Query {
	public static void main(String[] args) {
		//long users_id={1000000106l,1000000107l};
		//createNewConversation(users_id, "chat_name");
		//getUnreadMessages(126);
		//newMessage("DAtabase", 1000000106, 128);
	}


	public static void getAllUnreadMessages(long user_id){



		PreparedStatement proc;
		ArrayList<Message> history_info=new ArrayList<Message>();
		try {
			proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"getAllUnreadMessages\"(?);");


			proc.setObject(1, user_id);
			ResultSet rs=proc.executeQuery();
			rs.next();
			String postgre=rs.getString(1);
			//JSONArray jArray=new JSONArray(rs.getString(1));
			JSONArray jArray = new JSONArray("["+postgre.substring(1,postgre.length()-1)+"]");
			//System.out.println("["+postgre.substring(1,postgre.length()-1)+"]");
			for(int i=0;i<jArray.length();i++)
			{	
				JSONObject  message_obj=new JSONObject(jArray.getString(i));
				int convo_id=message_obj.getInt("convo_id");
				JSONArray msgs_jarray=message_obj.getJSONArray("msg_info");
				ArrayList<Message> alist_msgs = new ArrayList<Message>();
				Message current=new Message();
				for(int j=0;i<msgs_jarray.length();j++){
					JSONObject msg_jobj=msgs_jarray.getJSONObject(j);
					current.setId(msg_jobj.getInt("id"));
					current.setAuthor(msg_jobj.getInt("author"));
					current.setUsername(msg_jobj.getString("username"));
					current.setText(msg_jobj.getString("text"));

					current.setTime_stamp(new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSSSSS").parse(msg_jobj.getString("timestamp")).getTime()));
					alist_msgs.add(current);
				}
			}



			rs.close();
			proc.close();
			//writer.write(message_array.toString());
		} catch (JSONException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//	return history_info; 
	}


	public static void createNewConversation(Long users_id[],String chat_name){
		PreparedStatement proc;
		try {
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"newConversation\"(?,?);");
			proc.setArray(1, PostgreSQLConnection.getConnection().createArrayOf("bigint", users_id));
			proc.setString(2,chat_name);
			System.out.println(proc.toString());
			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




	}
	
	public static ArrayList<Conversation> getConversationInfo (long user_id) throws SQLException{
		PreparedStatement proc=null;
		proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT  * from public.\"getConversationsInfo\"(?);");
		proc.setLong(1,user_id);
		System.out.println(proc);
		 ResultSet rs = proc.executeQuery();
		
		rs.next();
		String postgre=rs.getString(1);
		
		JSONArray jArray=new JSONArray("["+postgre.substring(1,postgre.length()-1)+"]");
		
	    ArrayList<Conversation> convos=new ArrayList<Conversation>();
	   
	    for(int t=0;t<jArray.length();t++)
		{
		    String temp=jArray.get(t).toString();
		   
			JSONObject current_object=new JSONObject(temp.substring(1,temp.length()-1));
			
		
			Conversation convo=new Conversation();
			convo.setChat_name(current_object.getString("chatname"));
			convo.setConversation_id(current_object.getLong("conversation_id"));
			JSONArray j_array=current_object.getJSONArray("member_list");
			ArrayList<String> members=new ArrayList<String>();
			for(int i=0;i<j_array.length();i++){
		       members.add(j_array.getString(i));
			}
			convo.setMembers(members);
			convos.add(convo);
		}
			
		
		return convos;
	}
	
	
	public static ArrayList<Message> getUnreadMessages(long convo_id){
		PreparedStatement proc = null;
		ResultSet rs = null;
		ArrayList<Message> messages=new ArrayList<Message>();
	
			try {
				proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getUnreadMessages\"(?);");
			proc.setLong(1,convo_id);
			rs=proc.executeQuery();
			System.out.println(proc.toString());
			
			while(rs.next()){
				Message current=new Message();
				current.setId(rs.getInt("id"));
				current.setUsername(rs.getString("username"));
				current.setText(rs.getString("text"));
				current.setAuthor(rs.getInt("author"));
				System.out.println(rs.getString("text"));
				try {
					current.setTime_stamp(new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSSSSS").parse(rs.getString("message_timestamp")).getTime()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				messages.add(current);
			}
			
			

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
return messages;
		}
	
public static void newMessage(String message,long erpId,long conversation_id){
	PreparedStatement proc;
	 try {
		proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"newMessage\"(?,?,?);");
		proc.setString(1,message);
		proc.setLong(2,erpId);
		proc.setLong(3,conversation_id);
		System.out.println(proc.toString());
		 proc.executeQuery();
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
	
	 
	 public static void readAllMessages(long convo_id){
		 PreparedStatement proc;
		 try {
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"markAsReadMessages\"(?);");
			proc.setLong(1,convo_id);
			System.out.println(proc.toString());
			 proc.executeQuery();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	 }
public static ArrayList<Message> getChatMessages(long convo_id,int set,int limit){
	PreparedStatement proc = null;
	ResultSet rs = null;
	ArrayList<Message> messages=new ArrayList<Message>();
	
		try {
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getChatMessages\"(?,?,?);");
			proc.setLong(1,convo_id);
			proc.setInt(2,0);
			proc.setInt(3,1000);
			System.out.println(proc.toString());
			 rs=proc.executeQuery();
			while(rs.next())
			{	
				Message current=new Message();
				current.setId(rs.getInt("id"));
				current.setUsername(rs.getString("username"));
				current.setText(rs.getString("text"));
				current.setAuthor(rs.getInt("author"));
				try {
					current.setTime_stamp(new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSSSSS").parse(rs.getString("timestamp")).getTime()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				messages.add(current);
			}
			
					rs.close();
			proc.close();


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return messages;
		

}

}
		
		
		
	
    
	


