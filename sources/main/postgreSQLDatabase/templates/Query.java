/**
 * 
 */
package postgreSQLDatabase.templates;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import exceptions.IncorrectFormatException;
import postgreSQLDatabase.onlineTest.Answer;
import postgreSQLDatabase.templates.Template;
import settings.database.PostgreSQLConnection;
import users.Student;

/**
 * 
 * @author manisha pc
 *
 */
public class Query {
	static Connection conn;

	public static void main(String[] args) throws SQLException {
		getAllTemplates();

		ArrayList<String> ar = new ArrayList<String>();
		ar.add("ab");
		addTemplate("Mark", 34L, 1000000089L, ar);
		System.out.println("ERROR");

	}

	public static void addTemplate(String title, long file, Long author, ArrayList<String> tag) throws SQLException {
		PreparedStatement proc = PostgreSQLConnection.getConnection()
				.prepareStatement("SELECT public.\"addTemplates\"(?,?,?,?);");

		proc.setString(1, title);
		proc.setLong(2, file);
		proc.setLong(3, author);
		proc.setArray(4, PostgreSQLConnection.getConnection().createArrayOf("text", tag.toArray()));
		proc.executeQuery();
	}

	public static ArrayList<Template> getAllTemplates() {
		ArrayList<Template> template_list = null;
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getAllTemplates\"();");

			ResultSet rs = proc.executeQuery();

			template_list = new ArrayList<Template>();
			while (rs.next()) {
				Template templates = new Template();
				// templates.setId(rs.getLong("id"));
				templates.setFile(rs.getLong("file"));
				templates.setTitle(rs.getString("title"));
				templates.setAuthor(rs.getLong("author"));
				// templates.setTimestamp(rs.getDate("timestamp"));

				ResultSet as = rs.getArray("tags").getResultSet();
				ArrayList<String> tags = new ArrayList<String>();
				while (as.next()) {
					tags.add(as.getString(2));
					 
				}

				templates.setTags(tags);

				template_list.add(templates);
				System.out.println("hello");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return template_list;

	}

	public static ArrayList<Template> getTemplates(long id) {
		ArrayList<Template> template_list = null;
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getTemplates\"();");
			proc.setLong(1, id);
			ResultSet rs = proc.executeQuery();

			template_list = new ArrayList<Template>();
			while (rs.next()) {
				Template templates = new Template();
				// templates.setId(rs.getLong("id"));
				templates.setFile(rs.getLong("file"));
				templates.setTitle(rs.getString("title"));
				templates.setAuthor(rs.getLong("author"));
				// templates.setTimestamp(rs.getDate("timestamp"));
				ResultSet as = rs.getArray("tags").getResultSet();
				ArrayList<String> tags = new ArrayList<String>();
				while (as.next()) {
					tags.add(as.getString(1));
					// System.out.println(as.getString(1));

					templates.setTags(tags);
					template_list.add(templates);

				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return template_list;

	}

	public static void addTags(String tagname, String users, String attributes) {

		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"addTags\"(?,?,?);");
			proc.setString(1, tagname);
			proc.setString(2, users);
			proc.setString(3, attributes);
			proc.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static Tags getTags(String tag_name) {
		Tags tag = new Tags();
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getTags\"(?);");
			proc.setString(1, tag_name);
			ResultSet rs = proc.executeQuery();
			if(!rs.next())return null;
				
				tag.setUsers(rs.getString("users"));
				tag.setAttributes(rs.getString("attributes"));
                tag.setTagname(rs.getString("tagname"));
                tag.setId(rs.getLong("id"));
			
		}

		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tag;

	}

	public static long getFileId(String file_name) {
		long file_id = 0;
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFileId\"(?);");
			proc.setString(1, file_name);
			ResultSet rs = proc.executeQuery();
			rs.next();
			file_id = rs.getLong(1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file_id;

	}

	public static Student getStudentByStudentId(String std_id) {
		Student current=null;
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getStudentListById\"(?);");
			proc.setString(1, std_id);
			ResultSet rs = proc.executeQuery();
			rs.next();
			current=new Student(rs);
		return(current);	
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IncorrectFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return current;
	}

}
