/**
 * 
 */
package postgreSQLDatabase.documents;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;
import org.postgresql.util.PGobject;

import postgreSQLDatabase.feePayment.FeeBreakup;
import postgreSQLDatabase.file.File;
import settings.database.PostgreSQLConnection;

/**
 * @author Shubhi
 *
 */
public class Query {

	public static long addDocument(Documents document) throws SQLException {
		PreparedStatement proc = null;
		proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"addDocument\"(?,?,?);");

		proc.setLong(1, document.getFile_id());
		proc.setString(2, document.getDocument_name());
		proc.setLong(3, document.getOwner_id());
		
		ResultSet rs = proc.executeQuery();
		rs.next();
		return rs.getLong(1);

	}

	public static ArrayList<Documents> getDocuments(long reg_id) {
		PreparedStatement proc = null;
		ArrayList<Documents> getList = new ArrayList<Documents>();
		try {
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getDocuments\"(?);");
			proc.setLong(1, reg_id);
			ResultSet rs = proc.executeQuery();

			while (rs.next()) {
				Documents documents = new Documents();
				postgreSQLDatabase.file.File file=new File();
				
				documents.setDocument_name(rs.getString("title"));
				documents.setDocument_id(rs.getLong("document_id"));
				documents.setTimestamp(rs.getDate("file_timestamp"));
				documents.setFile_id(rs.getLong("file_id"));
				file.setAuthor(rs.getLong("author_id"));
				file.setDirectory(rs.getString("directory"));
				file.setExtension(rs.getString("extension"));
				file.setFile_name(rs.getString("file_name"));
				
				
				documents.setFile(file);
				getList.add(documents);
				
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return getList;
	}

	public static void main(String[] args) throws SQLException {
		ArrayList<Documents> list = getDocuments(278l);
		Iterator<Documents> iterator = list.iterator();
		while(iterator.hasNext()){
			Documents document = iterator.next();
		
			System.out.println(	document.getTimestamp());
		}
		

	}
}
