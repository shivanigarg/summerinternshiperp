/**
 * 
 */
package postgreSQLDatabase.documents;

import java.util.Date;

/**
 * @author Shubhi
 *
 */
public class Documents {
	private long file_id,owner_id,document_id;
	private String document_name;
	private Date timestamp;
	private postgreSQLDatabase.file.File file;
	
	/**
	 * @return the file_id
	 */
	public long getFile_id() {
		return file_id;
	}
	/**
	 * @return the document_id
	 */
	public long getDocument_id() {
		return document_id;
	}
	/**
	 * @param document_id the document_id to set
	 */
	public void setDocument_id(long document_id) {
		this.document_id = document_id;
	}
	/**
	 * @param file_id the file_id to set
	 */
	public void setFile_id(long file_id) {
		this.file_id = file_id;
	}
	/**
	 * @return the owner_id
	 */
	public long getOwner_id() {
		return owner_id;
	}
	/**
	 * @param owner_id the owner_id to set
	 */
	public void setOwner_id(long owner_id) {
		this.owner_id = owner_id;
	}
	/**
	 * @return the document_name
	 */
	public String getDocument_name() {
		return document_name;
	}
	/**
	 * @param document_name the document_name to set
	 */
	public void setDocument_name(String document_name) {
		this.document_name = document_name;
	}
	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	/**
	 * @return the file
	 */
	public postgreSQLDatabase.file.File getFile() {
		return file;
	}
	/**
	 * @param file the file to set
	 */
	public void setFile(postgreSQLDatabase.file.File file) {
		this.file = file;
	}

}
