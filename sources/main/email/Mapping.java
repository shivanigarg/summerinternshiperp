/**
 * 
 */
package email;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import exceptions.IncorrectFormatException;
import fileExplorer.FileExplorer;
import users.Student;

/**
 * @author Shubhi
 *
 */
public class Mapping {
	public static void main(String[] args) {
		System.out.println("reached");
		Student student = new Student();
		try {
			student.setName("Shivani");
			student.setStudent_id("2013kucp1001");
		} catch (IncorrectFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("{(Student-name)}", student.getName());
		map.put("{(Student-id)}", student.getStudent_id());
		map.put("{(Date)}", new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		map.put("{(Student-category)}", student.getCategory());
		// map.put("{(Student-semester)}",S(student.getSemester()));
		String string = fileExplorer.File.readFile("mapping.txt");
		System.out.println(string.length());
		// String string="Dear {(Student-name)} with dfghj {(Student-id)}
		// hjvdkbgn.Dear {(Student-name)} with dfghj {(Student-id)} hjvdkbgn.";

		TreeSet<String> tags = new TreeSet<String>(findTags(string));
		System.out.println(tags);
		String new_string = mapStudentDetails(string, map);

	}

	public static String mapStudentDetails(String string, HashMap<String, String> map) {
		StringBuilder sb = new StringBuilder(string);
		Pattern start = Pattern.compile("(\\{\\()");
		Pattern end = Pattern.compile("(\\)\\})");
		ArrayList<String> tags = new ArrayList<String>();
		while (true) {
			Matcher matcher1 = start.matcher(sb);
			Matcher matcher2 = end.matcher(sb);
			if (!matcher1.find()) {
				break;
			}
			int l = matcher1.start();
			matcher2.find();
			int e = matcher2.start();
			String key = sb.substring(matcher1.start(), matcher2.start() + 2);
			tags.add(key);
			System.out.println("key"+key);
			String value = map.get(key);
			System.out.println("value"+value);
			sb.replace(l, e + 2, value);

		}

		return sb.toString();
	}

	public static ArrayList<String> findTags(String string) {
		StringBuilder sb = new StringBuilder(string);
		int count = 0, length = 0;
		Pattern start = Pattern.compile("(\\{\\()");
		Pattern end = Pattern.compile("(\\)\\})");
		length = string.length();
		// Set<String> tags=new HashSet<String>();
		ArrayList<String> tags = new ArrayList<String>();
		while (true) {
			Matcher matcher1 = start.matcher(sb);
			Matcher matcher2 = end.matcher(sb);
			if (!matcher1.find()) {
				break;
			}
			int l = matcher1.start();
			matcher2.find();
			int e = matcher2.start();
			String key = sb.substring(matcher1.start(), matcher2.start() + 2);
			String k = key.substring(2, key.length() - 2);
			// System.out.println(k);
			tags.add(k);
			// System.out.println(key);
			String value = " ";

			sb.replace(l, e + 2, value);

		}

		return tags;
	}

}
