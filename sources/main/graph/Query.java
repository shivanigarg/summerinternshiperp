/**
 * 
 */
package graph;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONObject;

/**
 * @author Shubhi
 *
 */
public class Query {
	public static void main(String[] args) {
		try {
			//addUser("arushigupta",1000000093l,"student");
			//			addUser("joeypinto",1000000100l,"student");
			//			addUser("shivanigarg",1000000082l,"student");
			//				addUser("meghagupta",1000000106l,"student");
			//			connectFriends(1000000082l, 1000000100l);
			//connectFriends(1000000093l, 1000000106l);
			//ArrayList<User> alist=getFriends(1000000106l);
			Boolean isFriends=checkFriends(1000000106l,1000000093l);
			System.out.println(isFriends);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	public static void addUser(String username,Long erpId,String user_type) throws SQLException{

		PreparedStatement proc = graph.Graph.getConnection().prepareStatement("create(m:User{name:{1},erp_id:{2},user_type:{3}}) return m;");
		proc.setString(1,username);
		proc.setLong(2,erpId);
		proc.setString(3,user_type);
		System.out.println(proc.toString());
		ResultSet rs=proc.executeQuery();


		while(rs.next()) {
			System.out.println(rs.getObject(1));
			JSONObject json_object=new JSONObject(rs.getObject(1));
			System.out.println(json_object);
		}
	}

	public static void connectFriends(Long erpId1,Long erpId2) throws SQLException{
		if(checkFriends(erpId1, erpId2))return ;
		PreparedStatement proc = graph.Graph.getConnection().prepareStatement("match(n:User{erp_id:{1}}),(m:User{erp_id:{2}}) create (n)-[r:FRIEND_OF]->(m);");

		proc.setLong(1,erpId1);
		proc.setLong(2,erpId2);
		System.out.println(proc);
		ResultSet rs=proc.executeQuery();

		proc.setLong(2,erpId1);
		proc.setLong(1,erpId2);
		System.out.println(proc);
		rs=proc.executeQuery();


		while(rs.next()) {
			System.out.println(rs.getObject(1));
			JSONObject json_object=new JSONObject(rs.getObject(1));
			System.out.println(json_object);

		}
	}

	public static ArrayList<User> getFriends(Long erpId) throws SQLException{
		PreparedStatement proc = graph.Graph.getConnection().prepareStatement("match(n:User{erp_id:{1}})-[r:FRIEND_OF]->(m:User) return m;");
		proc.setLong(1,erpId);
		ResultSet rs=proc.executeQuery();
		//System.out.println(rs.getMetaData().getColumnCount());
		ArrayList<User> user_list=new ArrayList<User>();
		while(rs.next()) {
			User user=new User();
			Map<String,Object> current= (Map<String,Object>)rs.getObject(1);
			user.setName(current.get("name").toString());
			user.setErp_id(Long.parseLong(current.get("erp_id").toString()));
			user.setUser_type(current.get("user_type").toString());
			System.out.println(user.getErp_id()+" "+user.getName()+" "+user.getUser_type());
			user_list.add(user);

			//	JSONObject json_object=new JSONObject(rs.getObject(1));
			//	System.out.println(json_object);
			//	System.out.println(json_object.getLong("erp_id"));

		}
		return user_list;
	}

	public static boolean checkFriends(Long erpId1,Long erpId2) throws SQLException{
		PreparedStatement proc = graph.Graph.getConnection().prepareStatement("match(n:User{erp_id:{1}})-[r:FRIEND_OF]->(m:User{erp_id:{2}}) return count(r);");
		proc.setLong(1,erpId1);
		proc.setLong(2,erpId2);
		ResultSet rs=proc.executeQuery();
		rs.next();
		if((Integer)rs.getObject(1)==0){
			return false;
		}
		else
			return true;
	}
}
