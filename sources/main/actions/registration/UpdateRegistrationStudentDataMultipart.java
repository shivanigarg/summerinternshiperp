package actions.registration;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import exceptions.IncorrectFormatException;
import postgreSQLDatabase.registration.Query;
import users.Student;

/**
 * Servlet implementation class UpdateRegistrationStudent
 */
@MultipartConfig
@WebServlet("/UpdateRegistrationStudentDataMultipart")
public class UpdateRegistrationStudentDataMultipart extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateRegistrationStudentDataMultipart() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		DiskFileItemFactory factory = new DiskFileItemFactory();
		//factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

		ServletFileUpload upload = new ServletFileUpload(factory);
		PrintWriter writer=response.getWriter();
		Student student=new Student();   
		Long registration_id = null;
		try {
		
			List<FileItem> formItems = upload.parseRequest(request);
			System.out.println(formItems.size());

			Iterator<FileItem> iter = formItems.iterator();
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();
				if (item.isFormField()) {

					String field;
					try{
					switch(item.getFieldName()){
					
					
					case "update_first_name" :
						System.out.println("hello"+item.getString());
						student.setFirst_name(item.getString());
					break;
					case "registration_id" :
						registration_id=Long.parseLong(item.getString());
						student.setRegistration_id(Long.parseLong(item.getString()));
					break;
					//	 student.setRegistration_id(Long.parseLong(request.getParameter("update_registration_id").toString()));
					//	 System.out.println(request.getParameter("update_registration_id").toString());	

					case "update_middle_name" :
						student.setMiddle_name(item.getString());
						break;
					case "update_last_name":

						student.setLast_name(item.getString());
						break;
					case "update_guardian_name":
						//+"&category(request.getParameter("category"));
						student.setGuardian_name(item.getString());
						break;
					case "update_guardian_contact":

						student.setGuardian_contact(item.getString());
						break;
					case "update_guardian_email":

						student.setGuardian_email(item.getString());
						break;
					case "update_guardian_address" :
						student.setGuardian_address(item.getString());
						break;
					case "update_father_name" :

						student.setFather_name(item.getString());
						break;
					case "update_mother_name" :
						student.setMother_name(item.getString());
						break;
					case "update_father_contact" :
						student.setFather_contact(item.getString());
						break;
					case "update_mother_contact" :

						student.setMother_contact(item.getString());
						break;
						//+"&gender(request.getParameter("gender"));
						//+"&date_of_birth(request.getParameter("date_of_birth"));
						//+"&state_eligibility(request.getParameter("state_eligibility"));
						//+"&program_allocated(request.getParameter("program_allocated"));
					case "update_mobile" :
						student.setMobile(item.getString());
						break;
					case "update_email" :
						student.setEmail(item.getString());
						break;
					case "update_permanent_address" :
						student.setPermanent_address(item.getString());
						break;
					case "update_local_address" : 
						student.setLocal_address(item.getString());
						break;
					case "update_hosteller" :
						if(item.getString().equals("true"))
							student.setHosteller(true);
						else student.setHosteller(false);
						break;
					case "update_hostel" :

						student.setHostel(item.getString());
						break;
					case "update_hostel_room" :
						student.setRoom(item.getString());
						break;
					}
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}
		catch (Exception ex) {
			request.setAttribute("message", "There was an error: " + ex.getMessage());
			ex.printStackTrace();
		}





		try {




			Query.addUpdateStudentRegistrationDetails(student);
			Query.applyUpdate(registration_id);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		}	














	}


