package actions.cropProfile;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;


/**
 * Servlet implementation class CropProfile
 */

@WebServlet(
		name="Crop Profile Servlet",
		urlPatterns={"/CropProfile"}
	)
public class CropProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CropProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(500);
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		int t=Integer.parseInt(request.getParameter("t"));
		int l=Integer.parseInt(request.getParameter("l"));
		int w=Integer.parseInt(request.getParameter("w"));
		int h=Integer.parseInt(request.getParameter("h"));
		String imagePath=getServletContext().getRealPath("/")+request.getParameter("i");
		BufferedImage outImage=ImageIO.read(new File(imagePath));
		BufferedImage cropped=outImage.getSubimage(l, t, w, h);
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		ImageIO.write(cropped, request.getParameter("f"), out);

		ImageIO.write(cropped, request.getParameter("f"), new File(getServletContext().getRealPath("/")+System.getProperty("file.separator")
		+"cropped.jpg")); // save the file with crop dimensions

		//res.setContentType(�image/jpg�);
		ServletOutputStream wrt=response.getOutputStream();
		wrt.write(out.toByteArray());
		wrt.flush();
		wrt.close();
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}











