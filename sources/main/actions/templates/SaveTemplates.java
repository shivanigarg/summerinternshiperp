package actions.templates;

import users.User;
import java.io.BufferedReader;
//import postgreSQLDatabase.file;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import exceptions.IncorrectFormatException;
import exceptions.TagNotFoundException;
import postgreSQLDatabase.templates.Tags;

/**
 * Servlet implementation class SaveTemplates
 */
@WebServlet("/SaveTemplates")
public class SaveTemplates extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SaveTemplates() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		if (request.getParameter("contents") != null) {
			try {
				PrintWriter fileWriter = response.getWriter();
				String file = request.getParameter("contents");
				String uploadFilePath = request.getServletContext().getRealPath("") + File.separator + "templates";
				// System.out.println(uploadFilePath);
				File fileSaveDir = new File(uploadFilePath);
				if (!fileSaveDir.exists()) {
					fileSaveDir.mkdirs();
				}
				String filePath = uploadFilePath + File.separator + request.getParameter("name");

				postgreSQLDatabase.file.File obj = new postgreSQLDatabase.file.File();
				obj.setFile_name(request.getParameter("name"));
				postgreSQLDatabase.file.Query.getFileId(request.getParameter("name"));
				obj.setAuthor(Long.parseLong(session.getAttribute("erpId").toString()));
				obj.setDirectory("templates");
				obj.setExtension(".txt");
				System.out.println(filePath);
				fileExplorer.File.writeFile(file, filePath);
				long file_id=	postgreSQLDatabase.file.Query.addNewFile(obj);
				HashMap<String, String> map = new HashMap<String, String>();
				String file_contents = fileExplorer.File.readFile(filePath);
				ArrayList<String> tags = new ArrayList<String>();
				tags = email.Mapping.findTags(file_contents);
				Iterator<String> iterator = tags.iterator();

				while (iterator.hasNext()) {
					String tagname=iterator.next();
					Tags t = postgreSQLDatabase.templates.Query.getTags(tagname);
					if(t==null){
						TagNotFoundException e=new TagNotFoundException(tagname);
						throw e;
					} 
					
				}

					String title = request.getParameter("title");
					postgreSQLDatabase.templates.Query.addTemplate(title, file_id,Long.parseLong(session.getAttribute("erpId").toString()), tags);

				}

			 catch (FileNotFoundException e1) {
				e1.printStackTrace();

			} catch (SQLException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			catch (TagNotFoundException e3){
				e3.printStackTrace();

			}

		}

	}

}
