/**
 * 
 */
package users;

import java.sql.Date;

/**
 * @author Anita
 *
 */
public class User {
	private long id;
	private String username;
	private String user_type;
	private  Date last_seen;
	private String email;

	

	
	public String getUsername() {
		return username;
	}
	
	
	public String getUser_type() {
		return user_type;
	}
	
	public String getEmail() {
		return email;
	}
	
	public Date getLast_seen() {
		return last_seen;
	}
	
	
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setLast_seen(Date last_seen) {
		this.last_seen = last_seen;
	}
	
	
	
}
