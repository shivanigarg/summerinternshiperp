package forums;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import forum.Comment;
import forum.Post;
import postgreSQLDatabase.forum.Query;



/**
 * Servlet implementation class RefreshThread
 */
@WebServlet("/RefreshThread")
public class RefreshThread extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RefreshThread() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter writer=response.getWriter();
		Long thread_id=Long.parseLong(request.getParameter("thread_id"));
		//Long thread_id=Long.parseLong("2");
		ArrayList<Post> post_list=postgreSQLDatabase.forum.Query.getMultiplePosts(thread_id);
		Iterator<Post> post=post_list.iterator();
		JSONArray post_array = new JSONArray();
		JSONObject post_object;
		while (post.hasNext()) {
			post_object = new JSONObject();
			Post current = post.next();
			post_object.put("post_id", current.getPost_id());
			post_object.put("post_name", current.getPost_name());
			post_object.put("thread_id", thread_id);
			post_object.put("post_author_id", current.getAuthor_id());
			
			String post_author_name=postgreSQLDatabase.forum.Query.getAuthorName(current.getAuthor_id());
			post_object.put("post_author_name", post_author_name);
			String post_author_username=postgreSQLDatabase.authentication.Query.getUserUsername(current.getAuthor_id());
			if(current.getAuthor_id().equals(Long.parseLong(request.getSession().getAttribute("erpId").toString()))){
				post_object.put("post_author", true);
			}
			else{
				post_object.put("post_author",false);
			}
			post_object.put("post_author_username", post_author_username);
			post_object.put("timestamp",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(current.getTimestamp()));
			ArrayList<Comment> comment_list=Query.getMultipleComments(current.getPost_id());
			Iterator<Comment> comment=comment_list.iterator();
			JSONArray comment_array = new JSONArray();
			JSONObject comment_object;
			while(comment.hasNext()){
				comment_object = new JSONObject();
				Comment current1 = comment.next();
				comment_object.put("comment_id", current1.getComment_id());
				comment_object.put("data", current1.getData());
				String comment_author_name=Query.getAuthorName(current1.getAuthor_id());
				comment_object.put("comment_author_id",current1.getAuthor_id());
				comment_object.put("comment_author_name", comment_author_name);
				if(current1.getAuthor_id().equals(Long.parseLong(request.getSession().getAttribute("erpId").toString()))){
					comment_object.put("comment_author", true);
				}
				else{
					comment_object.put("comment_author",false);
				}
				comment_object.put("post_id", current1.getParent_post_id());
				comment_object.put("timestamp", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(current1.getTimestamp()));
				comment_array.put(comment_object);
			}
			post_object.put("comments",comment_array);
			post_array.put(post_object);
		}
		writer.write(post_array.toString());
		
	}

}
