package forums;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import postgreSQLDatabase.forum.Query;

/**
 * Servlet implementation class AddNewThread
 */
@WebServlet("/AddNewThread")
public class AddNewThread extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddNewThread() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String thread_name=request.getParameter("new_thread");
		HttpSession session=request.getSession();
		Long author_id=Long.parseLong(String.valueOf(session.getAttribute("erpId")));
		String post_name=request.getParameter("post_name");
		Long category_id=Long.parseLong(request.getParameter("category_id"));
		Long thread_id=Query.addThread(category_id, thread_name, author_id);
		Query.addPost(thread_id, author_id, post_name);
		
		
		response.sendRedirect("student/allPosts.jsp?thread_id="+thread_id);
		//System.out.println("added");
		
	}

}
