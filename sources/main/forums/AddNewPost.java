package forums;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import postgreSQLDatabase.forum.Query;

/**
 * Servlet implementation class AddNewPost
 */
@WebServlet("/AddNewPost")
public class AddNewPost extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddNewPost() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Long category_id=Long.parseLong(request.getParameter("category_id"));
		String post_name=request.getParameter("post_name");
		Long thread_id=Long.parseLong(request.getParameter("thread_id"));
		HttpSession session=request.getSession();
		Long author_id=Long.parseLong(String.valueOf(session.getAttribute("erpId")));
		Query.addPost(thread_id, author_id, post_name);
		response.sendRedirect("student/allPosts.jsp?thread_id="+thread_id);
	}

}
