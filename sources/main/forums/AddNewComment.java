package forums;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import postgreSQLDatabase.forum.Query;
import actions.authentication.Session;

/**
 * Servlet implementation class AddNewComment
 */
@WebServlet("/AddNewComment")
public class AddNewComment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddNewComment() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String data=request.getParameter("text");
		Long post_id=Long.parseLong(request.getParameter("post_id"));
		HttpSession session = request.getSession();
		System.out.println(data);
		Long author_id=Long.parseLong(String.valueOf(session.getAttribute("erpId")));
		System.out.println(author_id);
		Query.addComment(data,post_id,author_id);
		
		
	}

}
